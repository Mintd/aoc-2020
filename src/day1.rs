#[aoc_generator(day1)]
pub fn input_gen(input: &str) -> Vec<usize> {
	input.lines().map(|x| x.parse().unwrap()).collect()
}

#[aoc(day1, part1)]
pub fn solver1(input: &[usize]) -> usize {
	let pair = input
		.iter()
		.flat_map(|x| input.iter().map(move |y| (x, y)))
		.find(|(x, y)| (*x + *y) == 2020)
		.unwrap();

	pair.0 * pair.1
}

#[aoc(day1, part2)]
pub fn solver2(input: &[usize]) -> usize {
	let pair = input
		.iter()
		.flat_map(|x| {
			input
				.iter()
				.flat_map(move |y| input.iter().map(move |z| (x, y, z)))
		})
		.find(|(x, y, z)| (*x + *y + *z) == 2020)
		.unwrap();

	pair.0 * pair.1 * pair.2
}
