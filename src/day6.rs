use std::convert::From;

use enumflags2::BitFlags;

#[derive(BitFlags, Copy, Clone, Debug, PartialEq)]
#[repr(u32)]
pub enum Question {
	A = 0b10000000000000000000000000,
	B = 0b01000000000000000000000000,
	C = 0b00100000000000000000000000,
	D = 0b00010000000000000000000000,
	E = 0b00001000000000000000000000,
	F = 0b00000100000000000000000000,
	G = 0b00000010000000000000000000,
	H = 0b00000001000000000000000000,
	I = 0b00000000100000000000000000,
	J = 0b00000000010000000000000000,
	K = 0b00000000001000000000000000,
	L = 0b00000000000100000000000000,
	M = 0b00000000000010000000000000,
	N = 0b00000000000001000000000000,
	O = 0b00000000000000100000000000,
	P = 0b00000000000000010000000000,
	Q = 0b00000000000000001000000000,
	R = 0b00000000000000000100000000,
	S = 0b00000000000000000010000000,
	T = 0b00000000000000000001000000,
	U = 0b00000000000000000000100000,
	V = 0b00000000000000000000010000,
	W = 0b00000000000000000000001000,
	X = 0b00000000000000000000000100,
	Y = 0b00000000000000000000000010,
	Z = 0b00000000000000000000000001,
}

impl From<char> for Question {
	fn from(c: char) -> Self {
		match c {
			'a' => Question::A,
			'b' => Question::B,
			'c' => Question::C,
			'd' => Question::D,
			'e' => Question::E,
			'f' => Question::F,
			'g' => Question::G,
			'h' => Question::H,
			'i' => Question::I,
			'j' => Question::J,
			'k' => Question::K,
			'l' => Question::L,
			'm' => Question::M,
			'n' => Question::N,
			'o' => Question::O,
			'p' => Question::P,
			'q' => Question::Q,
			'r' => Question::R,
			's' => Question::S,
			't' => Question::T,
			'u' => Question::U,
			'v' => Question::V,
			'w' => Question::W,
			'x' => Question::X,
			'y' => Question::Y,
			'z' => Question::Z,
			_ => panic!("Tried to create Question with {}", c),
		}
	}
}

#[aoc_generator(day6)]
pub fn input_gen(input: &str) -> Vec<Vec<BitFlags<Question>>> {
	input
		.split("\n\n")
		.map(|group| {
			group
				.lines()
				.map(|person| individual_answers(person))
				.collect()
		})
		.collect()
}

#[aoc(day6, part1)]
pub fn part1(input: &[Vec<BitFlags<Question>>]) -> usize {
	input
		.iter()
		.map(|group| {
			group
				.iter()
				.fold(BitFlags::empty(), |acc, item| acc | *item)
				.iter()
				.count()
		})
		.sum()
}

#[aoc(day6, part2)]
pub fn part2(input: &[Vec<BitFlags<Question>>]) -> usize {
	input
		.iter()
		.map(|group| {
			group
				.iter()
				.fold(BitFlags::all(), |acc, item| acc & *item)
				.iter()
				.count()
		})
		.sum()
}

fn individual_answers(questions: &str) -> BitFlags<Question> {
	questions
		.chars()
		.fold(BitFlags::empty(), |acc, item| acc | Question::from(item))
}
