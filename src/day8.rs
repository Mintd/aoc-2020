use std::collections::HashSet;

#[derive(Clone, Copy, Debug)]
pub enum Instruction {
	ACC(isize),
	JMP(isize),
	NOP(isize),
}

#[aoc_generator(day8)]
pub fn input_gen(input: &str) -> Vec<Instruction> {
	input
		.lines()
		.map(|l| {
			let mut line = l.split(' ');

			let leading_instruction_char = line.next().unwrap().chars().next().unwrap();
			let parameter = line.next().unwrap().parse().unwrap();

			match leading_instruction_char {
				'a' => Instruction::ACC(parameter),
				'j' => Instruction::JMP(parameter),
				'n' => Instruction::NOP(parameter),
				_ => panic!("Unrecognized instruction"),
			}
		})
		.collect()
}

#[aoc(day8, part1)]
pub fn part1(input: &[Instruction]) -> isize {
	let mut pc = 0;
	let mut accumulator = 0;
	let mut visited_instructions = HashSet::with_capacity(input.len());

	while visited_instructions.get(&pc).is_none() {
		visited_instructions.insert(pc);

		match input[pc as usize] {
			Instruction::ACC(amount) => {
				accumulator += amount;
				pc += 1;
			}
			Instruction::JMP(amount) => pc += amount,
			Instruction::NOP(_) => pc += 1,
		}
	}

	accumulator
}

#[aoc(day8, part2)]
pub fn part2(input: &[Instruction]) -> isize {
	let mut visited_instructions = HashSet::with_capacity(input.len());
	let mut to_change = 0;
	let mut input = input.to_vec();

	'outer: loop {
		let mut accumulator = 0;
		let mut ir = 0;

		// find the next instruction that is not a "acc"
		let saved;
		loop {
			match input[to_change] {
				Instruction::ACC(_) => {
					to_change += 1;
					continue;
				}
				Instruction::JMP(amount) => {
					saved = input[to_change];
					input[to_change] = Instruction::NOP(amount);
				}
				Instruction::NOP(amount) => {
					saved = input[to_change];
					input[to_change] = Instruction::JMP(amount);
				}
			}

			break;
		}

		while visited_instructions.get(&ir).is_none() {
			if ir >= input.len() as isize {
				break 'outer accumulator;
			}

			visited_instructions.insert(ir);

			match input[ir as usize] {
				Instruction::ACC(amount) => {
					accumulator += amount;
					ir += 1;
				}
				Instruction::JMP(amount) => ir += amount,
				Instruction::NOP(_) => ir += 1,
			}
		}

		input[to_change] = saved;
		to_change += 1;
		visited_instructions.clear();
	}
}
