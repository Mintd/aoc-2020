#[aoc_generator(day9)]
pub fn input_gen(input: &str) -> Vec<usize> {
	input.lines().map(|l| l.parse().unwrap()).collect()
}

#[aoc(day9, part1)]
pub fn part1(input: &[usize]) -> usize {
	input
		.windows(26)
		.filter_map(|window| {
			let (current_value, previous) = window.split_last().unwrap();

			let result = previous
				.iter()
				.flat_map(|first| {
					previous
						.iter()
						.filter(move |value| *value != first)
						.map(move |second| *first + *second)
				})
				.find(|value| *value == *current_value);

			match result {
				Some(_) => None,
				None => Some(*current_value),
			}
		})
		.next()
		.unwrap()
}

#[aoc(day9, part2)]
pub fn part2(input: &[usize]) -> usize {
	let invalid = part1(input);
	let mut result = None;
	let mut window_size = 2;

	while result.is_none() {
		result = input
			.windows(window_size)
			.find(|window| window.iter().sum::<usize>() == invalid)
			.map(|window| *window.iter().min().unwrap() + *window.iter().max().unwrap());

		window_size += 1;
	}

	result.unwrap()
}
