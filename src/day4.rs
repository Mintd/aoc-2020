#[derive(Clone)]
pub struct Passport {
	birth_year: Option<String>,
	issue_year: Option<String>,
	expiry_year: Option<String>,
	height: Option<String>,
	hair_colour: Option<String>,
	eye_colour: Option<String>,
	passport_id: Option<String>,
}

pub enum Height {
	Metric(usize),
	Imperial(usize),
}

pub struct ValidPassport {
	birth_year: usize,
	issue_year: usize,
	expiry_year: usize,
	height: Height,
	hair_colour: String,
	eye_colour: String,
	passport_id: String,
}

impl Passport {
	fn new() -> Passport {
		Passport {
			birth_year: None,
			issue_year: None,
			expiry_year: None,
			height: None,
			hair_colour: None,
			eye_colour: None,
			passport_id: None,
		}
	}

	fn is_valid(&self) -> bool {
		self.birth_year.is_some()
			&& self.issue_year.is_some()
			&& self.issue_year.is_some()
			&& self.expiry_year.is_some()
			&& self.height.is_some()
			&& self.hair_colour.is_some()
			&& self.eye_colour.is_some()
			&& self.passport_id.is_some()
	}

	fn produce_valid_passport(self) -> Option<ValidPassport> {
		Some(ValidPassport {
			birth_year: self.birth_year?.parse().ok()?,
			issue_year: self.issue_year?.parse().ok()?,
			expiry_year: self.expiry_year?.parse().ok()?,
			height: {
				if let Some(h) = self.height.clone()?.strip_suffix("cm") {
					Height::Metric(h.parse().ok()?)
				} else {
					Height::Imperial(self.height?.strip_suffix("in")?.parse().ok()?)
				}
			},
			hair_colour: self.hair_colour?,
			eye_colour: self.eye_colour?,
			passport_id: self.passport_id?,
		})
	}
}

impl ValidPassport {
	fn is_valid(&self) -> bool {
		self.birth_year >= 1920
			&& self.birth_year <= 2002
			&& self.issue_year >= 2010
			&& self.issue_year <= 2020
			&& self.expiry_year >= 2020
			&& self.expiry_year <= 2030
			&& ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"].contains(&self.eye_colour.as_str())
			&& self.passport_id.len() == 9
			&& {
				let mut hair = self.hair_colour.chars();

				if hair.next() == Some('#') {
					let mut state = true;
					for _ in 0..6 {
						if let Some(c) = hair.next() {
							if !"0123456789abcdef".contains(c) {
								state = false;
								break;
							}
						}
					}
					state
				} else {
					false
				}
			} && {
			match self.height {
				Height::Metric(h) => h >= 150 && h <= 193,
				Height::Imperial(h) => h >= 59 && h <= 76,
			}
		}
	}
}

#[aoc_generator(day4)]
pub fn input_gen(input: &str) -> Vec<Passport> {
	input
		.split("\n\n")
		.map(|l| {
			let mut pass = Passport::new();

			for section in l.replace("\n", " ").split(' ') {
				let mut pair = section.split(':');

				match pair.next().unwrap() {
					"byr" => pass.birth_year = Some(pair.next().unwrap().into()),
					"iyr" => pass.issue_year = Some(pair.next().unwrap().into()),
					"eyr" => pass.expiry_year = Some(pair.next().unwrap().into()),
					"hgt" => pass.height = Some(pair.next().unwrap().into()),
					"hcl" => pass.hair_colour = Some(pair.next().unwrap().into()),
					"ecl" => pass.eye_colour = Some(pair.next().unwrap().into()),
					"pid" => pass.passport_id = Some(pair.next().unwrap().into()),
					_ => {}
				}
			}

			pass
		})
		.collect()
}

#[aoc(day4, part1)]
pub fn part1(input: &[Passport]) -> usize {
	input.iter().filter(|p| p.is_valid()).count()
}

#[aoc(day4, part2)]
pub fn part2(input: &[Passport]) -> usize {
	input
		.iter()
		.filter(|p| p.is_valid())
		.filter_map(|p| p.clone().produce_valid_passport())
		.filter(|vp| vp.is_valid())
		.count()
}
