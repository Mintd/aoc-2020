pub struct Password {
	num1: usize,
	num2: usize,
	letter: char,
	pass: String,
}

impl Password {
	fn solve1(&self) -> bool {
		(self.num1..=self.num2).contains(&self.pass.chars().filter(|c| self.letter.eq(c)).count())
	}

	fn solve2(&self) -> bool {
		let pos1 = self.pass.chars().nth(self.num1 - 1).unwrap() == self.letter;
		let pos2 = self.pass.chars().nth(self.num2 - 1).unwrap() == self.letter;

		pos1 != pos2
	}
}

#[aoc_generator(day2)]
pub fn input_gen(input: &str) -> Vec<Password> {
	input
		.lines()
		.map(|x| {
			let line: Vec<&str> = x.split_ascii_whitespace().collect();
			let (start, end) = parse_first_section(line[0]);

			Password {
				num1: start,
				num2: end,
				letter: line[1].chars().next().unwrap(),
				pass: line[2].to_string(),
			}
		})
		.collect()
}

#[aoc(day2, part1)]
pub fn part1(input: &[Password]) -> usize {
	input.iter().filter(|x| x.solve1()).count()
}

#[aoc(day2, part2)]
pub fn part2(input: &[Password]) -> usize {
	input.iter().filter(|x| x.solve2()).count()
}

fn parse_first_section(input: &str) -> (usize, usize) {
	let mut split_input = input.split('-');

	let start = split_input.next().unwrap().parse().unwrap();
	let end = split_input.next().unwrap().parse().unwrap();

	(start, end)
}
