use std::collections::{HashMap, HashSet};

use regex::Regex;

#[aoc(day7, part1)]
pub fn part1(input: &str) -> usize {
	let mut bags = HashMap::new();
	let rule =
		Regex::new(r"(?m)^(\w+ \w+) bags contain (\d+ \w+ \w+ bags?(?:, \d+ \w+ \w+ bags?)*)\.$")
			.unwrap();
	let bag_name = Regex::new(r"\d+ (\w+ \w+) bags?").unwrap();

	rule.captures_iter(input).for_each(|cap| {
		(&cap[2])
			.to_string()
			.split(", ")
			.map(|bag| {
				bag_name
					.captures(bag)
					.unwrap()
					.get(1)
					.unwrap()
					.as_str()
					.to_string()
			})
			.for_each(|colour| {
				bags.entry(colour)
					.or_insert_with(HashSet::new)
					.insert((&cap[1]).to_string());
			})
	});

	find_all_bags(&bags, "shiny gold".to_string())
}

fn find_all_bags(input: &HashMap<String, HashSet<String>>, bag_colour: String) -> usize {
	let mut bags = HashSet::new();
	let mut queue = Vec::new();
	queue.push(bag_colour);

	while let Some(bag) = queue.pop() {
		if let Some(containers) = input.get(&bag) {
			queue.extend(
				containers
					.iter()
					.cloned()
					.filter(|colour| !bags.contains(colour)),
			);

			bags.extend(containers.iter())
		}
	}

	bags.len()
}

#[aoc(day7, part2)]
pub fn part2(input: &str) -> usize {
	let mut bags: HashMap<String, Vec<(usize, String)>> = HashMap::new();
	let rule =
		Regex::new(r"(?m)^(\w+ \w+) bags contain (\d+ \w+ \w+ bags?(?:, \d+ \w+ \w+ bags?)*)\.$")
			.unwrap();
	let bag_re = Regex::new(r"(?P<num>\d+) (?P<colour>\w+ \w+) bags?").unwrap();

	rule.captures_iter(input).for_each(|cap| {
		bags.insert(
			(&cap[1]).to_string(),
			(&cap[2])
				.split(", ")
				.map(|bag_str| {
					let caps = bag_re.captures(bag_str).unwrap();

					(
						(&caps["num"]).parse().unwrap(),
						(&caps["colour"]).to_string(),
					)
				})
				.collect(),
		);
	});

	count_bags(&bags, String::from("shiny gold"))
}

fn count_bags(input: &HashMap<String, Vec<(usize, String)>>, bag_colour: String) -> usize {
	input.get(&bag_colour).map_or(0, |internal| {
		internal
			.iter()
			.map(|(count, new_colour)| *count + *count * count_bags(input, new_colour.to_string()))
			.sum()
	})
}
