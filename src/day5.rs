use std::collections::HashSet;

#[aoc_generator(day5)]
pub fn input_gen(input: &str) -> HashSet<u32> {
	input
		.lines()
		.map(|l| l.split_at(7))
		.map(|(row, column)| {
			let mut half_size = 64;
			let mut seat_row = 0;
			for half in row.chars() {
				if half == 'B' {
					seat_row += half_size;
				}
				half_size /= 2;
			}

			half_size = 4;
			let mut seat_column = 0;
			for half in column.chars() {
				if half == 'R' {
					seat_column += half_size;
				}
				half_size /= 2;
			}

			seat_row * 8 + seat_column
		})
		.collect()
}

#[aoc(day5, part1)]
pub fn part1(input: &HashSet<u32>) -> u32 {
	*input.iter().max().unwrap()
}

#[aoc(day5, part2)]
pub fn part2(input: &HashSet<u32>) -> u32 {
	let potential_seats: HashSet<u32> =
		(*input.iter().min().unwrap()..*input.iter().max().unwrap()).collect();

	*potential_seats.difference(input).next().unwrap()
}
