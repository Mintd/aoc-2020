#[aoc_generator(day10)]
pub fn input_gen(input: &str) -> Vec<usize> {
	let mut result: Vec<usize> = input.lines().map(|l| l.parse().unwrap()).collect();

	result.sort_unstable();
	result.push(result.last().unwrap() + 3);

	result
}

#[aoc(day10, part1)]
pub fn part1(input: &[usize]) -> usize {
	let mut one_jolt_diff = 0;
	let mut three_jolt_diff = 0;
	let mut last_jolt = 0;

	for jolt in input {
		match jolt - last_jolt {
			1 => one_jolt_diff += 1,
			3 => three_jolt_diff += 1,
			_ => {}
		}

		last_jolt = *jolt;
	}

	one_jolt_diff * three_jolt_diff
}

#[aoc(day10, part2)]
pub fn part2(input: &[usize]) -> usize {
	let mut memo = vec![0; input.last().unwrap() + 3];
	memo[*input.last().unwrap()] = 1;

	for jolt in input.iter().rev().skip(1) {
		memo[*jolt] = memo[jolt + 1] + memo[jolt + 2] + memo[jolt + 3];
	}

	memo[1] + memo[2] + memo[3]
}
