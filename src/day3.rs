#[derive(PartialEq)]
pub enum Space {
	Open,
	Tree,
}

#[aoc_generator(day3)]
pub fn input_gen(input: &str) -> Vec<Vec<Space>> {
	input
		.lines()
		.map(|x| {
			x.chars()
				.map(|point| {
					if point == '.' {
						Space::Open
					} else {
						Space::Tree
					}
				})
				.collect()
		})
		.collect()
}

#[aoc(day3, part1)]
pub fn part1(input: &[Vec<Space>]) -> usize {
	slope_solver(input, 3, 1)
}

#[aoc(day3, part2)]
pub fn part2(input: &[Vec<Space>]) -> usize {
	[(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]
		.iter()
		.map(|(x, y)| slope_solver(input, *x, *y))
		.product()
}

fn slope_solver(input: &[Vec<Space>], right: usize, down: usize) -> usize {
	let width = input[0].len();
	let mut x = 0;
	let mut y = 0;
	let mut trees = 0;

	while y < input.len() {
		if input[y][x] == Space::Tree {
			trees += 1;
		}

		x = (x + right) % width;
		y += down;
	}

	trees
}
